# Atlassian JIRA Docker

## Build image

`$ docker build -t alwynpan/docker-jira:7.7.1 .`


## Usage

### docker run

```
$ docker run --name jira --restart=always \
             -v /path/to/jira/home:/var/atlassian/jira \
             -p 8080:8080 \
             -e -X_PROXY_NAME=<domain name> \
             -e -X_PROXY_PORT=<80/443> \
             -e -X_PROXY_SCHEME=<http/https> \
             -e -X_PATH='' -d alwynpan/docker-jira:7.7.1
```

### docker-compose.yaml

```
version: "3"
services:
  postgres-jira:
    image: "postgres:9.5"
    restart: always
    volumes:
      - /path/to/jira/database:/var/lib/postgresql/data
    environment:
      - POSTGRES_PASSWORD=<password>

  jira:
    image: "alwynpan/docker-jira:7.4.1"
    restart: always
    volumes:
      - /path/to/jira/home:/var/atlassian/jira
    links:
      - postgres-jira
    ports:
      - "8080:8080"
    environment:
      - X_PROXY_NAME=<domain name>
      - X_PROXY_PORT=<80/443>
      - X_PROXY_SCHEME=<http/https>
      - X_PATH=
```
